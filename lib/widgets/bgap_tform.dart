import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class BGapTextForm extends StatelessWidget {
  final bool? enabled;
  final ValueChanged<String>? onChanged;
  final FormFieldValidator? validator;
  final TextEditingController? controller;
  final String hint;
  final TextInputType? keyboardType;
  const BGapTextForm({
    Key? key,
    this.enabled,
    this.onChanged,
    this.validator,
    this.controller,
    required this.hint,
    this.keyboardType,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: enabled,
      onChanged: onChanged,
      obscureText: true,
      cursorColor: Colors.grey,
      validator: validator,
      keyboardType: keyboardType,
      style: TextStyle(
        fontSize: 16.sp,
      ),
      controller: controller,
      decoration: InputDecoration(
        errorStyle: TextStyle(fontSize: 9.sp, height: 0.3),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(24.r),
          borderSide: const BorderSide(
            width: 1,
            color: Color(0xFF4A55A7),
          ),
        ),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(24.r),
          borderSide: const BorderSide(
            width: 1,
            color: Color(0xFF4A55A7),
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(24.r),
          borderSide: const BorderSide(
            width: 1,
            color: Color(0xFF4A55A7),
          ),
        ),
        disabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(24.r),
          borderSide: const BorderSide(
            width: 1,
            color: Color(0xFF4A55A7),
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(24.r),
          borderSide: const BorderSide(
            width: 1,
            color: Colors.redAccent,
          ),
        ),
        contentPadding: EdgeInsets.symmetric(
          horizontal: 16.w,
          vertical: 8.h,
        ),
        hintText: hint,
        hintStyle: TextStyle(
          fontSize: 16.sp,
        ),
      ),
    );
  }
}
