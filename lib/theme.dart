import 'package:flutter/material.dart';

var appThemeLight = ThemeData(
  fontFamily: 'Norm',
  hintColor: const Color(0xFF262931),
  colorScheme: const ColorScheme.light(
    background: Colors.white,
    onBackground: Colors.black,
  ),
  tabBarTheme: const TabBarTheme(
    unselectedLabelColor: Color(0xFF262931),
    labelColor: Colors.white,
    indicatorSize: TabBarIndicatorSize.tab,
  ),
  textTheme: const TextTheme(
    subtitle2: TextStyle(
      fontWeight: FontWeight.w500,
      height: 24 / 14,
      color: Color(0xFF262931),
      letterSpacing: -0.3,
      fontSize: 14,
    ),
  ),
);

var appThemeDark = ThemeData(
  fontFamily: 'Norm',
  hintColor: const Color(0xFFFFFFFF),
  colorScheme: const ColorScheme.dark(
    background: Color(0xFF1E1E1E),
    onBackground: Colors.white,
  ),
  tabBarTheme: const TabBarTheme(
    unselectedLabelColor: Colors.white,
    labelColor: Colors.white,
    indicatorSize: TabBarIndicatorSize.tab,
  ),
  textTheme: const TextTheme(
    subtitle2: TextStyle(
      fontWeight: FontWeight.w500,
      height: 24 / 14,
      color: Colors.white,
      letterSpacing: -0.3,
      fontSize: 14,
    ),
  ),
);
