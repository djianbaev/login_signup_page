import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:login/pages/login_page.dart';
import 'package:login/pages/login_store.dart';
import 'package:login/theme.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return Provider<LoginStore>(
      create: (_) => LoginStore(),
      child: MediaQuery(
        data: const MediaQueryData(),
        child: ScreenUtilInit(
          designSize: const Size(390, 844),
          builder: (context, child) => MaterialApp(
            title: 'BitsGap',
            theme: appThemeLight,
            darkTheme: appThemeDark,
            themeMode: ThemeMode.system,
            home: child,
          ),
          child: const LoginPage(),
        ),
      ),
    );
  }
}
