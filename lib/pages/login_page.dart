import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login/pages/home_page.dart';
import 'package:login/widgets/bgap_tform.dart';
import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';
import 'login_store.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  var emailController = TextEditingController(text: '');
  var passwordController = TextEditingController(text: '');
  var emailController2 = TextEditingController(text: '');
  var passwordController2 = TextEditingController(text: '');
  var usernameController = TextEditingController(text: '');
  final PageController _pageController = PageController(initialPage: 0);
  int _selected = 0;
  late LoginStore loginStore;
  late ReactionDisposer reactionDisposer;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    reactionDisposer();
    emailController.dispose();
    emailController2.dispose();
    passwordController.dispose();
    passwordController2.dispose();
    usernameController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    loginStore = Provider.of<LoginStore>(context);
    reactionDisposer = reaction((_) => loginStore.isLoggedIn, (bool loggedIn) {
      if (loggedIn) {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (_) => const HomePage()));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: 257.h,
                  decoration: BoxDecoration(
                    image: MediaQuery.of(context).platformBrightness ==
                            Brightness.light
                        ? const DecorationImage(
                            image: AssetImage('assets/images/bg_light.png'),
                          )
                        : const DecorationImage(
                            image: AssetImage('assets/images/bg_dark.png'),
                          ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.symmetric(
                    vertical: 54.h,
                    horizontal: 24.w,
                  ),
                  child: SvgPicture.asset('assets/images/logo.svg'),
                )
              ],
            ),
            const SizedBox(
              height: 73,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Column(children: [
                Container(
                  height: 201,
                  child: PageView(
                    controller: _pageController,
                    onPageChanged: (index) {
                      changePage(index, animate: true);
                    },
                    children: [
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 24.w),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Observer(
                                builder: (_) {
                                  return BGapTextForm(
                                    enabled: !loginStore.loading,
                                    onChanged: loginStore.setUsername,
                                    controller: emailController,
                                    keyboardType: TextInputType.text,
                                    validator: (value) {
                                      if (!loginStore.isFormValid) {
                                        return 'enter valid username';
                                      }
                                      return null;
                                    },
                                    hint: 'Username',
                                  );
                                },
                              ),
                              const SizedBox(height: 12),
                              Observer(builder: (_) {
                                return BGapTextForm(
                                  enabled: !loginStore.loading,
                                  onChanged: loginStore.setPassword,
                                  validator: (value) {
                                    if (!loginStore.isFormValid) {
                                      return 'enter valid password';
                                    }
                                    return null;
                                  },
                                  controller: passwordController,
                                  hint: 'Password',
                                );
                              }),
                            ],
                          ),
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 24.w),
                        child: Form(
                          key: _formKey2,
                          child: Column(
                            children: [
                              TextFormField(
                                cursorColor: Colors.grey,
                                style: TextStyle(
                                  fontSize: 16.sp,
                                ),
                                controller: usernameController,
                                keyboardType: TextInputType.emailAddress,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'enter valid username';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  errorStyle:
                                      TextStyle(fontSize: 9.sp, height: 0.3),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(24.r),
                                    borderSide: const BorderSide(
                                      width: 1,
                                      color: Color(0xFF4A55A7),
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(24.r),
                                    borderSide: const BorderSide(
                                      width: 1,
                                      color: Color(0xFF4A55A7),
                                    ),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(24.r),
                                    borderSide: const BorderSide(
                                      width: 1,
                                      color: Colors.redAccent,
                                    ),
                                  ),
                                  contentPadding: EdgeInsets.symmetric(
                                    horizontal: 16.w,
                                    vertical: 8.h,
                                  ),
                                  hintText: 'Email',
                                  hintStyle: TextStyle(
                                    fontSize: 16.sp,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 12),
                              TextFormField(
                                cursorColor: Colors.grey,
                                style: TextStyle(
                                  fontSize: 16.sp,
                                ),
                                controller: emailController2,
                                keyboardType: TextInputType.text,
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'enter valid email';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  errorStyle:
                                      TextStyle(fontSize: 9.sp, height: 0.3),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(24.r),
                                    borderSide: const BorderSide(
                                      width: 1,
                                      color: Color(0xFF4A55A7),
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(24.r),
                                    borderSide: const BorderSide(
                                      width: 1,
                                      color: Color(0xFF4A55A7),
                                    ),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(24.r),
                                    borderSide: const BorderSide(
                                      width: 1,
                                      color: Colors.redAccent,
                                    ),
                                  ),
                                  contentPadding: EdgeInsets.symmetric(
                                    horizontal: 16.w,
                                    vertical: 8.h,
                                  ),
                                  hintText: 'Username',
                                  hintStyle: TextStyle(
                                    fontSize: 16.sp,
                                  ),
                                ),
                              ),
                              const SizedBox(height: 12),
                              TextFormField(
                                obscureText: true,
                                cursorColor: Colors.grey,
                                style: TextStyle(
                                  fontSize: 16.sp,
                                ),
                                controller: passwordController2,
                                validator: (value) {
                                  if (value!.length < 6) {
                                    return 'enter valid password';
                                  }
                                  return null;
                                },
                                decoration: InputDecoration(
                                  errorStyle:
                                      TextStyle(fontSize: 9.sp, height: 0.3),
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(24.r),
                                    borderSide: const BorderSide(
                                      width: 1,
                                      color: Color(0xFF4A55A7),
                                    ),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(24.r),
                                    borderSide: const BorderSide(
                                      width: 1,
                                      color: Color(0xFF4A55A7),
                                    ),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(24.r),
                                    borderSide: const BorderSide(
                                      width: 1,
                                      color: Colors.redAccent,
                                    ),
                                  ),
                                  contentPadding: EdgeInsets.symmetric(
                                    horizontal: 16.w,
                                    vertical: 8.h,
                                  ),
                                  hintText: 'Password',
                                  hintStyle: TextStyle(
                                    fontSize: 16.sp,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Observer(builder: (_) {
                  return !loginStore.loading
                      ? Container(
                          margin: EdgeInsets.symmetric(
                              horizontal: 24.w, vertical: 30.h),
                          decoration: BoxDecoration(
                            color: MediaQuery.of(context).platformBrightness ==
                                    Brightness.light
                                ? const Color(0xFFBFDBFE)
                                : const Color(0xFF1E293B),
                            borderRadius: BorderRadius.circular(20.r),
                          ),
                          child: SizedBox(
                            height: 40,
                            child: Theme(
                              data: ThemeData(
                                highlightColor: Colors.transparent,
                                splashColor: Colors.transparent,
                              ),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Expanded(child: buildItem('Login', index: 0)),
                                  Expanded(
                                      child: buildItem('Sing-up', index: 1)),
                                ],
                              ),
                            ),
                          ),
                        )
                      : const Padding(
                          padding: EdgeInsets.all(15),
                          child: CircularProgressIndicator(),
                        );
                }),
              ]),
            ),
            GestureDetector(
              child: Text(
                'Forgot password?',
                style: TextStyle(
                  fontSize: 14.sp,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget buildItem(String title, {int index = 0}) {
    bool selected = _selected == index;
    return GestureDetector(
      onTap: () {
        changePage(index);
        switch (index) {
          case 0:
            // if (loginStore.isFormValid) {
            //   loginStore.login();
            // }
            if (_formKey.currentState != null) {
              if (_formKey.currentState!.validate()) {
                loginStore.login();
              }
            }
            break;
          case 1:
            if (_formKey2.currentState != null) {
              if (_formKey2.currentState!.validate()) {}
            }
            break;
        }
      },
      child: Container(
        alignment: Alignment.center,
        decoration: selected
            ? BoxDecoration(
                borderRadius: BorderRadius.circular(50),
                color: const Color(0xFF1A5CFF),
              )
            : null,
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 10),
        child: Text(title),
      ),
    );
  }

  changePage(int index, {bool animate = true}) async {
    await _pageController.animateToPage(index,
        duration: const Duration(milliseconds: 300), curve: Curves.decelerate);
    setState(() {
      _selected = index;
    });
  }
}
