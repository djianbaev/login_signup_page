import 'package:mobx/mobx.dart';
part 'login_store.g.dart';

class LoginStore = _LoginStoreBase with _$LoginStore;

abstract class _LoginStoreBase with Store {
  @observable
  bool loading = false;

  @action
  void login() async {
    loading = true;
    await Future.delayed(const Duration(seconds: 5));
    loading = false;
    isLoggedIn = true;
  }

  @observable
  String username = "";

  @action
  void setUsername(String value) => username = value;

  @observable
  String password = "";

  @action
  void setPassword(String value) => password = value;

  @computed
  bool get isFormValid => username.length > 6 && password.length > 6;

  @observable
  bool isLoggedIn = false;
}
